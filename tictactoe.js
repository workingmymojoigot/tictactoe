'use strict'
console.log('let\'s start codeing');


let playerTurn = 'X';
let count = 0;
let winner;
let matrix = [];
reset();

function boxClicked(x, y) {
    if (winner == false || winner == undefined){
        if (matrix[x][y] === undefined) {
            matrix[x][y] = playerTurn;
            document.getElementById(''+x+y+'').innerHTML = playerTurn;
            count += 1;
            if (count >= 5) {
                winner = checkWinner();
                if (winner != false) {
                    updateWinner(winner);
                } else if ( count == 9 && winner == false) {
                    // for draw scenario
                    document.getElementById("player-turn").innerHTML = "The Match is Draw, reset board";
                    document.getElementById("reset").style.display = "block";
                } else {
                    updatePlayerTurn(playerTurn);
                }
            } else if (count < 10){
                updatePlayerTurn(playerTurn);
            }
        } else if (winner == 'X' || winner == 'Y'){
            document.getElementById("player-turn").innerHTML = "player " + playerTurn + " this area is filled, try your luck on some other box" ;
        }
    }
}

function updatePlayerTurn(icon) {
    if (icon === 'X'){
        playerTurn = 'Y';
    } else {
        playerTurn = 'X';
    }
    document.getElementById("player-turn").innerHTML = playerTurn;
}

function checkWinner() {
    console.log(matrix);
    
    function checkHorizontal(i) {
        // when j == 2
        let first = matrix[i][0];
        
        if (first == undefined) {
            return false;
        }

        for (var j=1; j<3; j++) {
            if (matrix[i][j] != first) {
                return false;
            }
        }
        return first;
    }

    function checkVertical(j) {
        // when i ==2
        let first = matrix[0][j];

        if (first == undefined) {
            return false;
        }

        for (var i=1; i<3; i++) {
            if (matrix[i][j] != first) {
                return false;
            }
        }
        return first;

    }

    function checkDiagonal(j) {
        // when i == 2 and j == 0 or 2
        let first;
        
        if (j == 0) {
            first = matrix[0][2];
        } else {
            first = matrix[0][0];
        }

        if (first == undefined) {
            return false;
        }
        
        if (first == matrix[1][1]) {
            if (first == matrix[2][j]){
                return first;
            }
        }
        return false;
    }

    for (var i=0; i<3; i++) {
        for (var j=0; j<3; j++) {
            if (i == 2 ){
                winner = checkVertical(j);
                if (winner == false) {
                    if (j == 0 || j == 2 ) {
                        winner = checkDiagonal(j);
                        if (winner != false) {
                            return winner;
                        } else {
                            winner = checkHorizontal(i);
                            if (winner != false) {
                                return winner;
                            }
                        }
                    } else if(j == 1) {
                        winner = checkHorizontal(i);
                        if (winner != false) {
                            return winner;
                        }
                    }
                } else {
                    return winner;
                }
            } else if(j == 2) {
                winner = checkHorizontal(i);
                if (winner != false) {
                    return winner;
                }
            }
        }
    }
    return false;
}

function updateWinner(winner) {
    let player = winner; 
    document.getElementById("player-turn").innerHTML = player + "wins";
    document.getElementById("reset").style.display = "block";
}

function reset() {
    for(var i=0; i<3; i++) {
        matrix[i] = [];
        for(var j=0; j<3; j++) {
            matrix[i][j] = undefined;
            document.getElementById(''+i+j+'').innerHTML = '';
        }
    }
    winner = undefined;
    count = 0;
    playerTurn = 'X';
    document.getElementById("player-turn").innerHTML = playerTurn;
    document.getElementById("reset").style.display = "none";
}